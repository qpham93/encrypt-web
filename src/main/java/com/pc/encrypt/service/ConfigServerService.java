package com.pc.encrypt.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor
public class ConfigServerService {

  private final RestTemplate restTemplate = new RestTemplate();

  @Value("${config-server.get-encrypt-key-url}")
  private String urlGetMerchantEncryptionKey;

  public String getMerchantEncryptionKey(String merchantId, String merchantSubId) {
    try {
      Map<String, String> params = new HashMap<>();
      params.put("merchantId", merchantId);
      params.put("merchantSubId", merchantSubId);
      ResponseEntity<String> response = restTemplate
          .getForEntity(urlGetMerchantEncryptionKey, String.class, params);
      if (response.getStatusCode().equals(HttpStatus.OK)) {
        return response.getBody();
      }
    } catch (RestClientException e) {
      log.error("An error happened when call the config service to get the merchant private key", e);
      throw e;
    }

    return null;
  }
}
