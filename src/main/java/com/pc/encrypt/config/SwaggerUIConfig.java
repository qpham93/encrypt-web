package com.pc.encrypt.config;

import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerUIConfig {

  @Bean
  public OpenApiCustomiser openApiCustomiser() {
    return openApi -> {
    };
  }

}
