package com.pc.encrypt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EncryptWeb {

    public static void main(String[] args) {
        SpringApplication.run(EncryptWeb.class, args);
    }

}