package com.pc.encrypt.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IncomingForm {

  private String merchantId;

  private String merchantSubId;

  private String merchantUserId;

  private String merchantTxnNum;

  private String txnAmount;

  private String txnCurrency;

  private String firstName;

  private String lastName;

  private String phoneNumber;

  private String merchantCustomerEmail;

  private String returnUrl;

  private String iv;

  private String response;

  @Override
  public String toString() {
    return "{" +
        "\"merchant_id\": \"" + merchantId + "\"," +
        "\"merchant_sub_id\": \"" + merchantSubId + "\"," +
        "\"merchant_user_id\": \"" + merchantUserId + "\"," +
        "\"merchant_txn_num\": \"" + merchantTxnNum + "\"," +
        "\"txn_amount\": " + txnAmount + "," +
        "\"txn_currency\": \"" + txnCurrency + "\"," +
        "\"first_name\": \"" + firstName + "\"," +
        "\"last_name\": \"" + lastName + "\"," +
        "\"phone_number\": \"" + phoneNumber + "\"," +
        "\"merchant_customer_email\": \"" + merchantCustomerEmail + "\"," +
        "\"return_url\": \"" + returnUrl + "\"" +
        "}";
  }
}
