package com.pc.encrypt.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EncryptRequest {

  private String iv;
  private String text;
}
