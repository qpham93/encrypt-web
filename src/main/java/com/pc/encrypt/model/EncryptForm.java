package com.pc.encrypt.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EncryptForm {

  private String iv;
  private String text;
  private String result;
}
