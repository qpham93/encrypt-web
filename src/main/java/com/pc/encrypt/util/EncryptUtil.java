package com.pc.encrypt.util;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import org.springframework.stereotype.Component;

@Component
public class EncryptUtil {

  public static String encrypt(String text, String key, String iv)
      throws InvalidAlgorithmParameterException, NoSuchPaddingException,
      NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
    final var ivSpec = getIvSpec(iv);
    final var secretKey = getSecretKey( key );
    final var cipher = getCipher(Cipher.ENCRYPT_MODE, secretKey, ivSpec);

    byte[] encrypted = cipher.doFinal(text.getBytes(StandardCharsets.UTF_8));

    return Base64.getEncoder().encodeToString(encrypted);
  }

  public static String decrypt(String text, String key, String iv)
      throws NoSuchPaddingException, NoSuchAlgorithmException,
      BadPaddingException, IllegalBlockSizeException, InvalidAlgorithmParameterException, InvalidKeyException {
    final var ivSpec = getIvSpec(iv);
    final var secretKey  = getSecretKey(key);
    final var cipher = getCipher(Cipher.DECRYPT_MODE, secretKey, ivSpec);

    final var decodedText = Base64.getDecoder().decode(text);
    final var plainText = cipher.doFinal(decodedText);

    return new String(plainText);
  }

  private static SecretKey getSecretKey(String key) {
    return new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "AES");
  }

  private static IvParameterSpec getIvSpec(String iv) {
    return new IvParameterSpec(iv.getBytes(StandardCharsets.UTF_8));
  }

  private static Cipher getCipher(int opmode, SecretKey secretKey, IvParameterSpec ivSpec)
      throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException {
    final var cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");

    cipher.init(opmode, secretKey, ivSpec);

    return cipher;
  }
}
