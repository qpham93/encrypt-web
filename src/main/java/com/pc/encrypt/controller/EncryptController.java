package com.pc.encrypt.controller;

import com.pc.encrypt.model.EncryptForm;
import com.pc.encrypt.service.ConfigServerService;
import com.pc.encrypt.util.EncryptUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Controller
@RequestMapping("/encrypt")
@RequiredArgsConstructor
public class EncryptController {

  private final ConfigServerService configServerService;

  @GetMapping
  public String show(@ModelAttribute("form") EncryptForm encryptForm) {
    return "encrypt-data-form";
  }

  @PostMapping
  public String submit(@ModelAttribute("form") EncryptForm encryptForm) throws Exception {
    final String encryptionKey = configServerService.getMerchantEncryptionKey("", "");
    String encryptedData = EncryptUtil.encrypt(encryptForm.getText(), encryptionKey, encryptForm.getIv());
    encryptForm.setResult(encryptedData);

    return "encrypt-data-form";
  }
}