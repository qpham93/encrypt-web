package com.pc.encrypt.controller;

import com.pc.encrypt.model.IncomingForm;
import com.pc.encrypt.payload.InformationPayload;
import com.pc.encrypt.service.ConfigServerService;
import com.pc.encrypt.util.EncryptUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Controller
@RequestMapping
@RequiredArgsConstructor
public class IndexController {

  private final ConfigServerService configServerService;

  private final RestTemplate restTemplate = new RestTemplate();

  @Value("${ext-service.incoming-request-url}")
  private String ManualDepositServiceUrl;

  @GetMapping
  public String show(@ModelAttribute("form") IncomingForm form) {
    return "merchant-form";
  }

  @PostMapping
  public String submit(@ModelAttribute("form") IncomingForm form)
      throws InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
    final String encryptionKey = configServerService.getMerchantEncryptionKey("", "");
    final String detailEncrypt = EncryptUtil
        .encrypt(form.toString(), encryptionKey, form.getIv());
    InformationPayload informationPayload = InformationPayload.builder()
        .merchantId(form.getMerchantId())
        .details(detailEncrypt)
        .iv(form.getIv())
        .build();
    try {
      ResponseEntity<String> responseEntity = restTemplate
          .postForEntity(ManualDepositServiceUrl, informationPayload, String.class);
      if (responseEntity.getStatusCode().equals(HttpStatus.OK)) {
        form.setResponse(responseEntity.getBody());
      }
    } catch (Exception e) {
      form.setResponse(e.getMessage());
    }
    return "merchant-form";
  }
}