package com.pc.encrypt.controller;

import com.pc.encrypt.model.EncryptRequest;
import com.pc.encrypt.service.ConfigServerService;
import com.pc.encrypt.util.EncryptUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api")
@RequiredArgsConstructor
public class ApiController {

  private final ConfigServerService configServerService;

  @PostMapping("encrypt")
  public ResponseEntity<String> encrypt(@RequestBody EncryptRequest request) throws Exception {
    if (!StringUtils.hasText(request.getIv())
        || !StringUtils.hasText(request.getText())) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    final String encryptionKey = configServerService.getMerchantEncryptionKey("", "");
    String encryptedData = EncryptUtil.encrypt(request.getText(), encryptionKey, request.getIv());

    return new ResponseEntity<>(encryptedData, HttpStatus.OK);
  }
}